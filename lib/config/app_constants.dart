import 'package:flutter/material.dart';

abstract class AppConstants {
  static const String appName = 'Fun With Kanji';
  static const String appId = 'krillefear.funnykanji';
  static const String website = 'https://gitlab.com/KrilleFear/funny-kanji';
  static const Color fallbackPrimaryColor = Colors.brown;
}
